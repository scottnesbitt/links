# Open Source Links

For the techie and non techie: links around open source, and not just software.

  ---

## Ideas

* _[The Open Schoolhouse](http://theopenschoolhouse.com/)_ - An excellent and inspiring book that shows the impact how open source can have on students and schools
* [Public Domain is Not Open Source](https://opensource.org/node/878) - A brief, but interesting, analysis of the two ideas
* [Manifestos for the Internet Age](https://github.com/greyscalepress/) - The source files for book consisting of a set of manifestos focusing on computer culture
* [9 reasons to contribute to an open source project](http://www.datamation.com/open-source/slideshows/9-reasons-to-contribute-to-an-open-source-project.html) - You don't have to be a techie to do it, either
* [How open source has taken over our lives](http://www.cio.com/article/3179645/open-source-tools/how-open-source-has-take-over-our-lives.html) - And most people don't realize it
* [Open source takes on government 'black box' economics](https://thenewstack.io/tax-yor-brainzz-open-source-vs-government-black-boxes/) - Can open source lead to more open government?
* [Open source for all mankind](https://www.csmonitor.com/World/Passcode/Passcode-Voices/2016/0121/Opinion-Open-source-for-all-mankind) - Open source is about more than tech, it's _about self-determination_
* [The secret sauce to open source](https://opensource.cioreview.com/cxoinsight/the-secret-sauce-to-open-source-nid-24911-cid-92.html) - It's people, in case you're wondering. And not ina _Soylent Green_ sort of way, either

## Software

* [Awesome Linux Software](https://github.com/LewisVo/Awesome-Linux-Software) - A huge list of software for Linux
* [How to set up an all open source IT infrastructure from scratch](https://www.networkworld.com/article/3217713/open-source-tools/how-to-set-up-an-all-open-source-it-infrastructure-from-scratch.html) - A good overview of how to do just that
* [Manipulate Images with ImageMagick](http://www.linuxjournal.com/content/manipulate-images-imagemagick) - A good introductory tutorial from _Linux Journal_

## Linux

* [Compute Freely](https://computefreely.org/) - _Free your computer. Use Linux_
* [Half a dozen clever Linux command line tricks](https://www.networkworld.com/article/3219684/linux/half-a-dozen-clever-linux-command-line-tricks.html) - A set of potentially useful commands for the CLI junkie
* [Cracking the Shell](http://www.linuxinsider.com/story/84393.html) - A quick intro to the Linux command line
* [How to effectively clear your bash history](http://www.techrepublic.com/article/how-to-effectively-clear-your-bash-history/) - Use the command line? Here are some good tips for clearing the list of commands you've used
* [Podcasting with Linux command line tools and Audacity](https://www.packtpub.com/books/content/podcasting-linux-command-line-tools-and-audacity) - A solid tutorial, and not as techie as the title may lead you to believe it is
