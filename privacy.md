# Privacy Links

A set of links for anyone concerned with their personal and/or professional privacy.

   ---
   
* [Digital security tips for journalists: Protecting sources and yourself](https://journalistsresource.org/tip-sheets/reporting/digital-security-tips-protecting-sources-journalist) - Good tips, and not just for journalists
* [Detox Your Digital Self](https://myshadow.org/detox) - An eight-day plan to cleaning up your data trail online
* [What happens when you block digital giants](https://staltz.com/what-happens-when-you-block-internet-giants.html) - How one person ditched services like Google and Facebook, and what he used instead
* [Privacy Tools](https://www.privacytools.io/) - The _knowledge and tools to protect your privacy against global mass surveillance_
* [searx.me](https://searx.me/) - A search engine that respects your privacy
* [IndieWebify.me](https://indiewebify.me/) - A guide to taking control of what you publish online
* [Defending Accounts Against Common Attacks](https://source.opennews.org/guides/defending-accounts/) - A curated set of links to information about keeping your online accounts safe
* [Why end-to-end encryption is about more than just privacy](https://www.helpnetsecurity.com/2017/09/13/end-to-end-encryption/) - It can also protect businesses and us
