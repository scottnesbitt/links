# Emacs Links

A set of links for the Emacs user, and wannbe Emacs user. These links focus on Emacs packages, advice for using the editor, and on [org-mode](http://orgmode.org/).

  ---

## General Links

* [Emacs Life](http://emacslife.com/) - A great list of resources for the Emacs user
* [Links to Emacs sites on the web](http://dotemacs.de/links.html) - A slightly older, though still useful, collection of links
* [The Ultimate Collection of Emacs Resources](http://batsov.com/articles/2011/11/30/the-ultimate-collection-of-emacs-resources/) - You can be the judge of that ...
* [Awesome Emacs](https://github.com/sefakilic/awesome-emacs) - _Yet another curated list of resources for Emacs_
* [Pragmatic Emacs](http://pragmaticemacs.com/) - A collection of useful tips for the Emacs user
* [Emacs cheatsheet](https://www2.warwick.ac.uk/fac/sci/dcs/people/research/csriaq/teaching/cs118-emacs.pdf) - A PDF of a good cheatsheet from the University of Warwick

## Tutorials

* [Absolute Beginner's Guide to Emacs](http://www.jesshamrick.com/2012/09/10/absolute-beginners-guide-to-emacs/) - A solid tutorial that will get you up and running quickly
* [How to Learn Emacs](http://sachachua.com/blog/2013/05/how-to-learn-emacs-a-hand-drawn-one-pager-for-beginners/) - A set of one-page visual guides
* [Getting Started with GNU Emacs](http://www.zerny.dk/emacs/getting-started-with-emacs.html) - Another helpful quick start guide
* [Emacs: A Tutorial for Beginners](http://www.tuxradar.com/content/emacs-tutorial-beginners) - A longer, slightly techie tutorial
* [Woodnotes Emacs Guide for Writers](http://therandymon.com/woodnotes/emacs-for-writers/emacs-for-writers.html) - A good introduction to Emacs, by a writer for writers
* [How to write a book in Emacs](https://www.masteringemacs.org/article/how-to-write-a-book-in-emacs) - By someone who did
* [Getting started with the Emacs text editor](https://opensource.com/life/16/2/intro-to-emacs) - A good, quick introduction to Emacs
* [How to Use the Emacs Editor in Linux](https://www.digitalocean.com/community/tutorials/how-to-use-the-emacs-editor-in-linux) - A solid introductory tutorial on getting up and running with Emacs
* [Writing a book with the help of Emacs and friends](https://procomun.wordpress.com/2014/03/10/writing-a-book-with-emacs/) - A good intro on how to do just that

## Emacs packages

* [A community-driven list of Emacs packages](https://github.com/emacs-tw/awesome-emacs) - A good collection of add-ons for the editor
* [Deft for Emacs](http://jblevins.org/projects/deft/) - A useful package for creating and managing notes
* [Olivetti mode](https://github.com/rnkn/olivetti) - Creates a nicer, distraction-free environment for writing
* [Flyspell](http://www-sop.inria.fr/members/Manuel.Serrano/flyspell/flyspell.html) - An on-the-fly spelling checker for Emacs
* [html-helper-mode](http://www.nongnu.org/baol-hth/index.html) - A useful package if you work with HTML
* [Company mode](https://company-mode.github.io/) - Auto completes various kinds of text
* [Planner mode](https://www.emacswiki.org/emacs/PlannerMode) - A useful personal information manager
* [Neo Tree](https://www.emacswiki.org/emacs/NeoTree#toc10) - A directory tree add-on for Emacs

## org-mode Links

* [David O'Toole Org tutorial](http://orgmode.org/worg/org-tutorials/orgtutorial_dto.html) - A comprehensive walkthrough of org-mode
* [Org-mode basics: exporting your notes](http://pragmaticemacs.com/emacs/org-mode-basics-v-exporting-your-notes/) - How to convert notes in org-mode to another file format
* [Org-mode Tutorial/Cheatsheet](https://emacsclub.github.io/html/org_tutorial.html) - A handy guide to the basics of org-mode
* [Org as a Word Processor](http://www.howardism.org/Technical/Emacs/orgmode-wordprocessor.html) - How to make org-mode files more visually appealing
* [Org mode: beginning at the basics](http://orgmode.org/worg/org-tutorials/org4beginners.html) - A good tutorial for anyone new to org-mode
* [Org-mode markup cheatsheet](http://ergoemacs.org/emacs/emacs_org_markup.html) - Useful for those times when you can't remember how to format something
* [Getting Organized with org-mode](https://www.youtube.com/playlist?list=PLVtKhBrRV_ZkPnBtt_TD1Cs9PJlU0IIdE) - A good set of videos that show you how to use org-mode as a productivity tool
