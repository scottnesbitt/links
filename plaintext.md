# Plain Text Links

A collection of links that show you how to be more productive using plain text files.

   ---

## General Links

* [The Plain Text Project](https://plaintextproject.online) - My site explaining how plain text can make your life easier and more efficient
* [A Plain Text Primer](http://bettermess.com/a-plain-text-primer/) - A quick guide to getting started with plain text
* [8 Everyday Things You Can Track with Text Files](http://www.makeuseof.com/tag/8-everyday-things-can-track-text-files/) - _Storing data in text files wherever possible is a shortcut to a simpler workflow_
* [Why Geeks Love Plain Text (And Why You Should Too)](http://www.lifehack.org/articles/technology/why-geeks-love-plain-text-and-why-you-should-too.html) - Plain text isn't just for the techie
* [9 Good Reasons Why You Should use Plain Text Files](https://blog.davidberti.com/9-good-reasons-why-should-you-use-plain-text-files/) - These are many of the same reasons why I use plain text
* [Plain Text Accounting](http://plaintextaccounting.org/) - Yes, you can keep track of your money with plain text and a command line app
* [When plain text is wrong](http://brettterpstra.com/2011/10/03/when-plain-text-is-wrong/) - As useful as it is, plain text does have its limits

## Productivity

* [Plaintext Productivity](http://plaintext-productivity.net/) - A guide to using plain text on Windows
* [Why Use a Plain-Text File for Your Todos?](http://widefido.com/blog/why-use-a-plain-text-file-for-your-todos/) - A good explanation of why you should use plain text for your task list
* [WorkingMemory.txt (The Most Important Productivity Tool You’ve Never Heard Of)](http://calnewport.com/blog/2015/10/27/deep-habits-workingmemory-txt-the-most-important-productivity-tool-youve-never-heard-of/) - How to use a plain text file to remember all the little administrative tasks you need to tackle
* [Using Plain Text as a To Do List](http://besttodolist.org/using-plain-text-files-as-a-to-do-list) - A simple system that uses just a text editor and, optionally, a smartphone app
* [Productivity in plaintext](https://lukespear.co.uk/plaintext-productivity) - A long-ish post about how to get working in plain text
* [The unsexy plain text to-do file](https://medium.com/@sverrirv/the-unsexy-plain-text-todo-file-8b4f77d835e) - What it lacks in sexiness, it makes up for in utility
* [A Plain Text Personal Organizer](http://danlucraft.com/blog/2008/04/plain-text-organizer/) - An interesting way of staying organized with plain text
* [https://zapier.com/blog/plain-text-files-for-productivity/](https://zapier.com/blog/plain-text-files-for-productivity/) - A look at how text files can help you become more organized and efficient
* [How I organize everything with plain-text notes](https://www.macworld.com/article/1168148/software-productivity/how-i-organize-everything-with-plain-text-notes.html) - The tools are Mac-centric, but you can apply to information in this article to any set of similar tools on any operating system
* [The future of education is plain text](https://simplystatistics.org/2017/06/13/the-future-of-education-is-plain-text/)
* [Why you need a plain text resume to apply for jobs online](http://www.wisebread.com/why-you-need-a-plain-text-resume-to-apply-for-jobs-online)

## Writing

* [5 Unexpected Benefits of Plain Text Files for Writers](http://becomeawritertoday.com/plain-text/) - Good advice, and not just for writers
* [Typesetting Automation](http://mrzool.cc/writing/typesetting-automation/) - A somewhat techie workflow that can produce nicely-formatted documents
* [Plain Text for Authors and Writers](http://dooling.com/index.php/2012/12/20/plain-text-for-authors-writers/) - A bit long, but a decent argument for using plain text to write
* [Plain Text Will Boost Your Productivity as a Writer](https://ulyssesapp.com/blog/2015/06/plain-text-productivity/) - At least, it can boost your writing productivity
* [The Plain Text Worklfow](https://richardlent.github.io/post/the-plain-text-workflow/) - A useful workflow for writing and publishing
* [How and Why to Dump Your Word Processor](http://www.alandmoore.com/blog/2012/08/08/how-and-why-to-dump-your-word-processor/) - I agree with this, though I keep a word processor handy (LibreOffice Writer, in case you're wondering)
* [Plain Text for Writers](https://matthewlowes.com/2011/10/08/plain-text-for-writers-part-i-an-argument-for-the-use-of-plain-text/) - _An Argument for the Use of Plain Text_

## Note Taking

* [A Simple, Flexible System for Taking Notes](http://www.madebymark.com/2011/04/20/2011420a-simple-flexible-system-for-taking-notes-html/) - A _system for taking, searching, and retrieving notes_
* [Plain Text Note System](https://jonbeebe.net/2017/07/plain-text-notes-system/) - How one person uses plain text for note taking
* [How to Set Up a Text-Based Notes System](https://computers.tutsplus.com/tutorials/quick-tip-how-to-set-up-a-text-based-notes-system--mac-49142) - A tutorial focused on using a Mac, but which you can apply to any operating system
* [Goodbye Evernote. Hello Plain Text Notes](http://www.rubenbejar.com/2013/04/goodbye-evernote-hello-synchronized-markdown-plain-text-notes/) - How one person moved away from Evernote, using plain text and open source tools
