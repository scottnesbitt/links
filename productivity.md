# Productivity Links

A set of links to information that can help you better organize yourself and keep your work on track.

   ---
   
* [How to Create an Effective Schedule](https://www.philnewton.net/blog/how-to-create-an-effective-schedule/) - A blog post explaining the whys and hows of creating a simple schedule for yourself
* [The Chokehold of Calendars](https://medium.com/@monteiro/the-chokehold-of-calendars-f70bb9221b36) - Use them to schedule work, not interruptions
* [How to Beat Procrastination](https://hbr.org/2016/07/how-to-beat-procrastination) - Good advice that can help you do just that
* [The Road to Burnout is Paved with Context Switching](http://sophieshepherd.com/2017/06/14/learning-to-be-chill.html) - On the dangers of constantly switching between tasks
* [The Real Reason You're Not Finishing Your To-Do List](http://blog.scribblepost.com/real-reason-youre-not-finishing-list/) - Maybe you're structuring it wrong
* [The mind-clearing magic of Japan's pen-and-paper "planner culture](https://qz.com/712334/more-than-a-schedule-not-quite-a-diary-inside-japans-joyful-all-paper-planner-culture/) - A look at how going analog can help make you more efficient and productive
* [Why You Should Stop Reading About Productivity](https://medium.com/human-output/why-you-should-stop-reading-about-productivity-5c412c2ae1d3) - And actually __do__ things
* [How ignoring people makes you more efficient](http://www.nzherald.co.nz/lifestyle/news/article.cfm?c_id=6&objectid=11600561) - It's a powerful way to keep your focus.
* [Letting Go of Distractions](https://zenhabits.net/distractions/) - Good advice from Leo Babauta
* [Stop being productive](https://joebuhlig.com/stop-being-productive/) - You don't have to work all of the time
* [How to Tackle Your To-do List When You're Tired](https://productivityist.com/todolist-tired/) - Tips and techniques that will help you to be _productive regardless of energy and time available_
* [How to Avoid the Busy Trap](https://tim.blog/2016/03/21/avoid-the-busy-trap/) - Useful tips from Tim Ferriss
* [Until You Have Productivity Skills, Productivity Tools Are Useless](https://hbr.org/2016/08/until-you-have-productivity-skills-productivity-tools-are-useless) - Something I've been saying for years ...
* [The Unbusy Manifesto](https://qz.com/845419/jonathan-fields-the-unbusy-manifesto-how-to-slow-down-chill-out-and-live-life-more-intentionally/) - _How to slow down, chill out, and live life more intentionally_
* [8 Clever Time Management Skills For Engineers](https://www.workflowmax.com/blog/8-clever-time-management-skills-for-engineers) - And they're not just useful for engineers
* [How to turn stress and panic into productivity](https://crew.co/blog/stress-and-panic-into-productivity/) - One can actually lead to the other
* [The real trick to productivity](https://www.articulatemarketing.com/blog/real-trick-to-productivity-techniques) - And that's exercising _emotional discipline_
* [Stop looking for the perfect system](http://unexecutive.com/perfect-system/) - Just find the productivity system that works best for you
* [What Productivity Systems Won't Solve](https://zenhabits.net/unsolved/) - Those problems go deeper than mere tools can go
* [Why Creating a To-Do List is Derailing Your Success](https://www.fastcompany.com/3056852/why-creating-a-to-do-list-is-derailing-your-success) - Instead, turn your calendar _into an effective blueprint for the day_
* [How to Do One Thing at a Time—and Stop Multitasking](https://business.tutsplus.com/tutorials/how-to-do-one-thing-at-a-time-and-stop-multitasking--cms-25159) - Because our brains aren't designed for multitasking, no matter what some people tell you
* [Prioritization – More Important Than Any Productivity Technique](http://www.productivity501.com/prioritization-wins/9438/) - Productivity isn't about doing more. It's about doing what you need to do more effectively.

