# Links

This is a set of links on various topics of that interest me and which I hope will interest you. I regularly update this repository, so keep checking back.

I've decided to slap a [Creative Commons CC0-1.0](https://creativecommons.org/publicdomain/zero/1.0/) public domain license on this because ... well, you can't really copyright links, can you?

* [Emacs Links](emacs.md)
* [Journalism Links](journalism.md)
* [Open Source Links](opensource.md)
* [Plain Text Links](plaintext.md)
* [Privacy Links](privacy.md)
* [Productivity Links](productivity.md)
