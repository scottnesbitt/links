# Journalism Links

A set of links related to journalism, including [data journalism](https://en.wikipedia.org/wiki/Data_journalism).

 ---

## Writing and Reporting

* [Interviewing a source](https://journalistsresource.org/tip-sheets/reporting/interviewing-a-source) - Some good tips on how to do just that
* [Top journalists reveal the best reporting advice they have received](https://www.cjr.org/special_report/margaret-sullivan-fahrenthold-ioffe-ben-smith-gay-talese-steve-coll.php) - Good tips, even if you aren't a reporter
* [10 format ideas for short-form audio storytelling](https://www.journalism.co.uk/skills/10-format-ideas-for-short-form-audio-storytelling/s7/a699697/) - Some good ways to get the story out in audio form
* [How to write great captions for your photos](http://www.poynter.org/2015/how-to-write-great-captions-for-your-photos/344643/) - It's amazing how much a good caption can enhance a photo
* [How to use open source information to investigate stories online](http://ijnet.org/en/blog/how-use-open-source-information-investigate-stories-online) - It's amazing how much information is floating around out there

## Skills

* [Math for journalists](http://www.journaliststoolbox.org/category/writing-with-numbers/) - A good set of resources for any writer who needs to work with numbers, but has trouble with them when they run out of fingers and toes
* [The benefits of coding skills in the newsroom](https://www.journalism.co.uk/news/get-with-the-program-the-benefits-of-coding-skills-in-the-newsroom/s2/a678470/) - Coding, even if it's just HTML and CSS, is a good skill for today's journalists to know
* [Cyber security for journalists](http://www.bbc.co.uk/blogs/collegeofjournalism/entries/ffc98743-7ac1-4032-b1b1-9f237b27be70?ns_mchannel=social&ns_campaign=bbc_college&ns_source=twitter&ns_linkname=corporate) - Tips that can help you secure your communication

## Data Journalism

* [The Data Journalism Handbook](http://datajournalismhandbook.org/1.0/en/index.html) - A solid resource for the budding data journalist
* "[The story doesn't end with a spreadsheet](https://www.journalism.co.uk/video/-the-story-doesn-t-end-with-a-spreadsheet-advice-for-journalists-working-with-data/s400/a689044/)" - _Advice for journalists working with data_
* [How to find 'feeds for leads' as a journalist](https://onlinejournalismblog.com/2016/09/26/how-to-find-feeds-for-leads-as-a-journalist/) - Advice for digging up information, in some places you least expect
* [Digging into digital images: Extracting batch location data automatically](https://exposingtheinvisible.org/resources/obtaining-evidence/image-digging) - Great tips for verifying photographs
* [How to do data journalism on a budget](https://www.journalism.co.uk/skills/how-to-do-data-journalism-on-a-budget/s7/a554684/) - Data journalism doesn't (always) require a big team and a lot of cash
* [Free software and open source tools for investigative journalism and journalistic research](https://www.mandalka.name/investigative_journalism/) - Some good tools for getting the job done, while not breaking the bank
* [Scraping web data](https://exposingtheinvisible.org/guides/scraping/) - A good guide to collecting data off the web from the folks at Exposing the Invisible

## General Links

* [Is “platform” the right metaphor for the technology companies that dominate digital media?](http://www.niemanlab.org/2017/08/is-platform-the-right-metaphor-for-the-technology-companies-that-dominate-digital-media/) - Platform, it seems, is too vague a term
* [Journalists need their own archives. Here's how to start one](https://onlinejournalismblog.com/2016/02/19/how-to-create-personal-archive-caches-pinboard/) - Things have come a long way from the paper archives I used to keep ...
* [How Solutions Journalism Can Unearth Social Sector Knowledge](https://thewholestory.solutionsjournalism.org/hidden-in-plain-sight-how-solutions-journalism-can-unearth-social-sector-knowledge-5c181124687c) - How to fix our problems could be in documents no one reads
* [Beyond the parachute: Newsrooms rethink centralized model](https://www.cjr.org/innovations/parachute-journalism-newsroom-bureaus.php) - On breaking out of journalism's unfortunate geographic concentration
* [GitHub tutorials and resources for journalists](https://www.poynter.org/news/github-tutorials-and-resources-journalists) - Sometimes, even journalists need to embrace their inner geeks
* [Wire for journalists](https://medium.com/@wireapp/wire-for-journalists-protecting-sources-when-traveling-ec678b462ed) - An intro to using the Wire secure messaging app to protect your sources while travelling

